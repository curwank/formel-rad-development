package ch.bbw.cucw.formelrad;

/**
 * Calculator Class
 * @authors Claudio Weckherlin & Celina Urwank
 * @version 28.09.2018
 */

public class Calculator {
	private double leistung;
	private double spannung;
	private double strom;
	private double widerstand;
	
	public Calculator(double leistung, double spannung, double strom, double widerstand) {
		super();
		this.leistung = leistung;
		this.spannung = spannung;
		this.strom = strom;
		this.widerstand = widerstand;
	}
	
	public double getLeistung() {
		return leistung;
	}
	
	public double getSpannung() {
		return spannung;
	}

	public double getStrom() {
		return strom;
	}

	public double getWiderstand() {
		return widerstand;
	}

	@Override
	public String toString() {
		return "Calculator [leistung=" + leistung + 
				", spannung=" + spannung + 
				", strom=" + strom + 
				", widerstand="	+ widerstand + "]";
	}

	public void calculate() {

		if (leistung != 0.0) {
			if (spannung != 0.0) {

				// Call Methods with P and U
				strom = iAusPdurchU(leistung, spannung);
				widerstand = rAusUhoch2durchP(spannung, leistung);

			} else if (strom != 0.0) {

				// Call Methods with P and I
				spannung =  uAusPundI(leistung, strom);
				widerstand = rAusPdurchIhoch2(leistung, strom);


			} else if (widerstand != 0.0) {

				// Call Methods with P and R
				spannung = uAusPundR(leistung, widerstand);
				strom = iAusPdurchR(leistung, widerstand);

			}
		} else if (spannung != 0.0) {
			if (strom != 0.0) {

				// Call Methods with U and I
				leistung = pAusUundI(spannung, strom);
				widerstand = rAusUdurchI(spannung, strom);

			} else if (widerstand != 0.0) {

				// Call Methods with U and R
				leistung = pAusUundR(spannung, widerstand);
				strom = iAusUdurchR(spannung, widerstand);
			}
		} else if (strom != 0.0) {
			if (widerstand != 0.0) {

				// Call Methods with I and R
				spannung = uAusRundI(widerstand, strom);
				leistung = pAusRundI(widerstand, strom);
			}
		}

	}


	/* ========================================
	* Calculate U (Volt)
	* ========================================
	* */
	public double uAusRundI(double r, double i) {
		return r * i;
	}

	public double uAusPundI(double p, double i) {
		return p / i;
	}

	public double uAusPundR(double p, double r) {
		return Math.sqrt(p * r);
	}


	/* ========================================
	 * Calculate P (Power)
	 * ========================================
	 * */
	public double pAusUundI(double u, double i) {
		return u * i;
	}

	public double pAusRundI(double r, double i) {
		return r * (Math.pow(i, 2));
	}

	public double pAusUundR(double u, double r) {
		return (Math.pow(u, 2)) / r;
	}


	/* ========================================
	 * Calculate I (Ampere)
	 * ========================================
	 * */
	public double iAusPdurchR(double p, double r) {
		return Math.sqrt(p / r);
	}

	public double iAusPdurchU(double p, double u) {
		return p / u;
	}

	public double iAusUdurchR(double u, double r) {
		return u / r;
	}


	/* ========================================
	 * Calculate R (Ohm)
	 * ========================================
	 * */
	public double rAusUdurchI(double u, double i) {
		return u / i;
	}

	public double rAusPdurchIhoch2(double p, double i) {
		return p / (Math.pow(i, 2));
	}

	public double rAusUhoch2durchP(double u, double p) {
		return (Math.pow(u, 2)) / p;
	}

}
