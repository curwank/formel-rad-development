package ch.bbw.cucw.formelrad;

import java.awt.image.BufferedImage;
import java.io.*;

import javafx.application.Application;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;


import javax.imageio.ImageIO;

/**
 * Main Application Class
 * @authors Claudio Weckherlin & Celina Urwank
 * @version 28.09.2018
 */

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			Pane root = new Pane();

			// Creating an image
			Image image = new Image(new FileInputStream("src/ch/bbw/cucw/formelrad/assets/formelrad.gif"));
			ImageView imageView = new ImageView(image);
			imageView.setX(40);
			imageView.setY(20);
			imageView.setFitHeight(300);
			imageView.setFitWidth(300);
			imageView.setPreserveRatio(true);
			root.getChildren().add(imageView);

			Label lbleistung = new Label("Leistung:");
			lbleistung.relocate(20, 310);
			lbleistung.setFont(Font.font(15));
			root.getChildren().add(lbleistung);

			TextField txLeistung = new TextField();
			txLeistung.relocate(160, 310);
			txLeistung.setFont(Font.font("Verdana", 15));
			root.getChildren().add(txLeistung);

			Label lblSpannung = new Label("Spannung:");
			lblSpannung.relocate(20, 355);
			lblSpannung.setFont(Font.font(15));
			root.getChildren().add(lblSpannung);

			TextField txSpannung = new TextField();
			txSpannung.relocate(160, 355);
			txSpannung.setFont(Font.font("Verdana", 15));
			root.getChildren().add(txSpannung);

			Label lblStrom = new Label("Strom:");
			lblStrom.relocate(20, 400);
			lblStrom.setFont(Font.font(15));
			root.getChildren().add(lblStrom);

			TextField txStrom = new TextField();
			txStrom.relocate(160, 400);
			txStrom.setFont(Font.font("Verdana", 15));
			root.getChildren().add(txStrom);

			Label lblWiderstand = new Label("Widerstand:");
			lblWiderstand.relocate(20, 445);
			lblWiderstand.setFont(Font.font(15));
			root.getChildren().add(lblWiderstand);

			TextField txWiderstand = new TextField();
			txWiderstand.relocate(160, 445);
			txWiderstand.setFont(Font.font("Verdana", 15));
			root.getChildren().add(txWiderstand);

			Button btnBerechnen = new Button();
			btnBerechnen.relocate(160, 495);
			btnBerechnen.setText("Berechnen");
			root.getChildren().add(btnBerechnen);

			Button btnClear = new Button();
			btnClear.relocate(285, 495);
			btnClear.setText("Löschen");
			root.getChildren().add(btnClear);

			Label lblWarning = new Label();
			lblWarning.relocate(20, 555);
			lblWarning.setFont(Font.font("Verdana", 15));
			lblWarning.setTextFill(Color.web("ff0000"));
			root.getChildren().add(lblWarning);

			btnBerechnen.setOnAction(e -> {

				double p = 0.0;
				double u = 0.0;
				double i = 0.0;
				double r = 0.0;

				if(!txLeistung.getText().isEmpty()) {

					p = Double.parseDouble(txLeistung.getText());
					System.out.println("Die Leistung beträgt " + "\033[31;1m" + txLeistung.getText() + "\033[0m");

				} else {
					txLeistung.setStyle("-fx-text-fill: red;");
				}

				if(!txSpannung.getText().isEmpty()) {

					u = Double.parseDouble(txSpannung.getText());
					System.out.println("Die Spannung beträgt " + "\033[31;1m" + txSpannung.getText() + "\033[0m");

				} else {
					txSpannung.setStyle("-fx-text-fill: red;");
				}

				if(!txStrom.getText().isEmpty()) {

					i = Double.parseDouble(txStrom.getText());
					System.out.println("Der Strom beträgt " + "\033[31;1m" + txStrom.getText() + "\033[0m");

				} else {
					txStrom.setStyle("-fx-text-fill: red;");
				}

				if(!txWiderstand.getText().isEmpty()) {

					r = Double.parseDouble(txWiderstand.getText());
					System.out.println("Der Widerstand beträgt " + "\033[31;1m" + txWiderstand.getText() + "\033[0m");

				} else {
					txWiderstand.setStyle("-fx-text-fill: red;");
				}

				if (
						!txLeistung.getText().isEmpty() && !txSpannung.getText().isEmpty() && !txStrom.getText().isEmpty() ||
								!txLeistung.getText().isEmpty() && !txSpannung.getText().isEmpty() && !txWiderstand.getText().isEmpty() ||
								!txSpannung.getText().isEmpty() && !txStrom.getText().isEmpty() && !txWiderstand.getText().isEmpty() ||
								!txSpannung.getText().isEmpty() && !txStrom.getText().isEmpty() && !txWiderstand.getText().isEmpty() && !txLeistung.getText().isEmpty()
				) {
					lblWarning.setText("WARNING!\nSie haben mehr als 2 Grössen eingegeben!");
				} else {
					lblWarning.setText("");
				}

				Calculator myCalculator = new Calculator(p, u, i, r);
				myCalculator.calculate();

				if(txLeistung.getText().isEmpty()) {
					System.out.println("Die berechnete Leistung beträgt " + "\033[31;1m" + myCalculator.getLeistung() + "\033[0m");
				}

				if(txSpannung.getText().isEmpty()) {
					System.out.println("Die berechnete Spannung beträgt " + "\033[31;1m" + myCalculator.getSpannung() + "\033[0m");
				}

				if(txStrom.getText().isEmpty()) {
					System.out.println("Der berechnete Strom beträgt " + "\033[31;1m" + myCalculator.getStrom() + "\033[0m");
				}

				if(txWiderstand.getText().isEmpty()) {
					System.out.println("Der berechnete Widerstand beträgt " + "\033[31;1m" + myCalculator.getWiderstand() + "\033[0m");
				}

				System.out.println();
				System.out.println();

				System.out.print("Vorher:  ");
				System.out.println(myCalculator.toString());

				System.out.print("Nachher: ");
				System.out.println(myCalculator.toString());

				txLeistung.setText(Double.toString(myCalculator.getLeistung()));
				txSpannung.setText(Double.toString(myCalculator.getSpannung()));
				txStrom.setText(Double.toString(myCalculator.getStrom()));
				txWiderstand.setText(Double.toString(myCalculator.getWiderstand()));


			});

			btnClear.setOnAction(e -> {

				txLeistung.setText("");
				txLeistung.setStyle("-fx-text-fill: black;");

				txSpannung.setText("");
				txSpannung.setStyle("-fx-text-fill: black;");

				txStrom.setText("");
				txStrom.setStyle("-fx-text-fill: black;");

				txWiderstand.setText("");
				txWiderstand.setStyle("-fx-text-fill: black;");

				if (!lblWarning.getText().isEmpty()) {
					lblWarning.setText("");
				}

			});

			Scene scene = new Scene(root, 380, 615);
			primaryStage.setScene(scene);
			primaryStage.setResizable(false);
			primaryStage.setTitle("Formelrad");
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}
}
